#include <iostream>

using namespace std;

class Dni {
    public:
        Dni(int num, char letra) {
            this->num = num;
            this->letra = letra;
        }

        void setNum(int numeroNuevo) {
            // int numeroNuevo = 826789;
            num = numeroNuevo;
        }

        void setLetra(char letra) {
            this->letra = letra;
        }

        int getNum () {
            return num;
        }

        char getLetra() {
            return letra;
        }
        
    private:
        int num;
        char letra;
};

int main () {

    Dni fichaDni(43463610, 'G');

    //fichaDni.num = 239; -- No se puede ejecutar pq 'num' es privado
    cout << "El número es " << fichaDni.getNum() << " y la letra es " << fichaDni.getLetra() << endl;

    // Cambiar Dni a 8230942 y letra 'A'
    fichaDni.setNum(826789);
    fichaDni.setLetra('A');

    // Volver a imprimir la información del objeto
    cout << "El número es " << fichaDni.getNum() << " y la letra es " << fichaDni.getLetra() << endl;
    
    return 0;
}
